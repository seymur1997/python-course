def move (f,t):
    print("Move from ",f, "to ",t,"!")
    
def hanoi (ndiscs,a,b,c):
    "from (a), with help of (b), to (c)"
    if ndiscs == 0:
        pass
    else:
        hanoi(ndiscs-1,a,c,b)
        move(a,c)
        hanoi(ndiscs-1,b,a,c)

#Program Code    

ndiscs=int(input("How many discs?"))
hanoi(ndiscs,'A','B','C')