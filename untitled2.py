capitals = {
    "Argentina": "Buenos Aires",
    "Australia": "Canberra",
    "Azerbaijan": "Baku",
    "Brazil": "Brasilia",
    "Canada": "Ottawa",
    "Denmark": "Copenhagen",
    "Egypt": "Cairo",
    "France": "Paris",
    "Germany": "Berlin",
    "Iceland": "Reykjavik",
    "India": "New Delhi",
    "Italy": "Rome",
    "Japan": "Tokyo",
    "Kenya": "Nairobi",
    "Lebanon": "Beirut",
    "Morocco": "Rabat",
    "Nepal": "Kathmandu",
    "Portugal": "Lisbon",
    "Romania": "Bucharest",
    "Russia": "Moscow",
    "Scotland": "Edinburgh",
    "Spain": "Madrid",
    "Sweden": "Stockholm",
    "Switzerland": "Bern",
    "Thailand": "Bangkok",
    "Turkey": "Ankara",
    "United Kingdom": "London",
    "United States": "Washington D.C.",
    "Vietnam": "Hanoi",
    "Zimbabwe": "Harare",
}

import random

points=0
while True:
    country=random.choice(list(capitals.keys()))
    ans=input(f"What is the capital of (country)?")
    if ans.casefold()==capitals[country].casefold():
        points+=1
        print ("Nice one")
    else:
        print (f"Wrong! The right answer"
               f"is (capitals[country])")
        opt = input ("Another one [y/n]?")
        if opt.casefold()=="n":
            break
        print (f"total points=[points]")