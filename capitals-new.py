capitals = {
    "Argentina": "Buenos Aires",
    "Australia": "Canberra",
    "Azerbaijan": "Baku",
    "Brazil": "Brasilia",
    "Canada": "Ottawa",
    "Denmark": "Copenhagen",
    "Egypt": "Cairo",
    "France": "Paris",
    "Germany": "Berlin",
    "Iceland": "Reykjavik",
    "India": "New Delhi",
    "Italy": "Rome",
    "Japan": "Tokyo",
    "Kenya": "Nairobi",
    "Lebanon": "Beirut",
    "Morocco": "Rabat",
    "Nepal": "Kathmandu",
    "Portugal": "Lisbon",
    "Romania": "Bucharest",
    "Russia": "Moscow",
    "Scotland": "Edinburgh",
    "Spain": "Madrid",
    "Sweden": "Stockholm",
    "Switzerland": "Bern",
    "Thailand": "Bangkok",
    "Turkey": "Ankara",
    "United Kingdom": "London",
    "United States": "Washington D.C.",
    "Vietnam": "Hanoi",
    "Zimbabwe": "Harare",
}

import random

points=0
done = False
while not done:
    country=random.choice(list(capitals.keys()))
    capital = capitals[country]
    ans=input(f"What is the capital of {country}?")
    if ans.casefold()==capital.casefold():
        points+=1
        print ("Nice one")
#         done = True
    else:
        print (f"Wrong! The right answer is {capital}")
    opt = input ("Another one [y/n]?")
    if opt.casefold()=="n":
        print (f"total points={points}")
        done = True
        