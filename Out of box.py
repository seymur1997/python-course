next_n = lambda a: a+1
prev_n = lambda a: a-1
def add (a,n):
    if n==0:
        return a
    else:
        return next_n (add (a, prev_n(n)))
    
def mult (a,n):
        if n==0:
            return 0
        else:
            return add (a, mult (a, prev_n(n)))

def power (a,n):
    if n ==0:
        return 1
    else:
        return mult (a, power(a, prev_n(n)))


def BinarySearch (L, value):
   
 def BS(L, value, low, high):
    if high<low:
       return None
  mid = (low+high)//2
  if L[mid]==value:
      return mid
  elif L[mid]>value:
      return BS(L[0:mid], value, low, mid-1)
  elif L[mid]<value:
      return BS(L[mid+1:], value, mid+1, high)
  else:
      return None
  return BS(L, value, 0, len(L)-1)