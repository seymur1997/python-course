def nroot(arg, n=2):
    """This function returns the n-root of
    a number; defaults to square root"""
    return arg**(1/n)
print(nroot(729,3))